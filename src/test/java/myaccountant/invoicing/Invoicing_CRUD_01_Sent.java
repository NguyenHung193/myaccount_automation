package myaccountant.invoicing;

import static commons.AbstractTest.service;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import commons.AbstractTest;
import commons.PageFactoryManage;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import myaccountant.pageObjects.ActivityPageObject;
import myaccountant.pageObjects.AddCustomerPageObject;
import myaccountant.pageObjects.AddIncomeExpensePaymentPageObject;
import myaccountant.pageObjects.AddInvoicePageObject;
import myaccountant.pageObjects.AddItemPageObject;
import myaccountant.pageObjects.AuthenticationPageObject;
import myaccountant.pageObjects.DashboardPageObject;
import myaccountant.pageObjects.EditTransactionPageObject;
import myaccountant.pageObjects.InvoicePageObject;
import myaccountant.pageObjects.LoginPageObject;
import myaccountant.pageObjects.ReportPageObject;
import myaccountant.pageObjects.SettingsPageObject;
import myaccountant.pageObjects.SignUpPageObject;
import testData.ReportPageData;
import testData.SwipeBarMenuData;

public class Invoicing_CRUD_01_Sent extends AbstractTest {

	private AndroidDriver<AndroidElement> driver;
	AuthenticationPageObject authenticationPage;
	SignUpPageObject signUpPage;
	DashboardPageObject dashboardPage;
	AddIncomeExpensePaymentPageObject addIncomeExpensePaymentPage;
	ActivityPageObject activityPage;
	EditTransactionPageObject editTransactionPage;
	SettingsPageObject settingsPage;
	AddItemPageObject addItemPage;
	AddCustomerPageObject addCustomerPage;
	AddInvoicePageObject addInvoicePage;
	InvoicePageObject invoicePage;

	String email = "hung.nguyen.enouvotest+" + "invoicing" +randomNumber() + "@gmail.com";
	String firstName, lastName, phoneNumber, password, confirmPassword, verifyCode;
	String incomeCash, expenseCash, incomeCredit, expenseCredit, paymentMountIn, paymentMountOut;
	String newAmount;

	@BeforeTest
	public void beforeTest() {

		incomeCash = "1000";
		expenseCash = "2000";
		incomeCredit = "500";
		expenseCredit = "300";
		paymentMountIn = "600";
		paymentMountOut = "800";

		killAllNodes();

		service = startServer();

		driver = openAndroidDevice("MyAccountantStaging");

		authenticationPage = PageFactoryManage.getAuthenticationPage(driver);

		firstName = "Hung";
		lastName = "Nguyen";
		phoneNumber = "0905495922";
		password = "Hung0905495922";
		confirmPassword = "Hung0905495922";

	}

	@Test
	public void TC_01_LoginToSystem() {

		log.info("LoginPage: Creating new account and login");

		signUpPage = authenticationPage.clickToSignUpButton();

		signUpPage.sendkeyToDynamicSignUpInput(firstName, "First name");

		signUpPage.sendkeyToDynamicSignUpInput(lastName, "Last name");

		signUpPage.sendkeyToDynamicSignUpInput(email, "Email");

		signUpPage.sendkeyToDynamicSignUpInput(phoneNumber, "Phone number");

		signUpPage.sendkeyToDynamicSignUpInput(password, "Password");

		signUpPage.sendkeyToDynamicSignUpInput(confirmPassword, "Confirm Password");

		signUpPage.clickToNextButton();

		signUpPage.waitForMailWasSent();

		String str = "<span style=\"font-size: 22pt\">";
		String a = verifyMail("hung.nguyen.enouvotest@gmail.com", "Hung0905495922", "MyAccountant Email Verification");
		String b = a.substring(a.indexOf(str) + str.length(),
				a.indexOf("<span style=\"font-size: 22pt\">") + 6 + str.length());
		System.out.println("VerifyCode: " + b);
		verifyCode = b;

		signUpPage.sendkeyToDynamicSignUpInput(verifyCode, "Verify code");

		signUpPage.clickToVerifynButton();

		dashboardPage = signUpPage.clickToSkipItButton();
	}

	@Test
	public void TC_02_EntrySentInvoice() {

		log.info("Add Invoicing add-ons");

		dashboardPage.clickToMenuButton(driver);

		settingsPage = (SettingsPageObject) dashboardPage.clickToDynamicMenuLink(driver, "Settings");

		settingsPage.sendkeyToDynamicMenuSettingsLink("Add-ons");

		settingsPage.clickToEnableInvoicingButton();

		settingsPage.backToSettings();
		
		log.info("Add new item");

		settingsPage.clickToMenuButton(driver);

		invoicePage = (InvoicePageObject) settingsPage.clickToDynamicMenuLink(driver, "Invoicing");

		invoicePage.clickToFabButton();

		addItemPage = invoicePage.clickToAddItemButton();

		addItemPage.sendkeysToName("Hung");

		addItemPage.sendkeysToPrice("1000");

		addItemPage.selectCategory();

		addItemPage.selectDynamicCategory("Bank Interest");

		addItemPage.submitTransaction();
		
		log.info("Add new customer");

		invoicePage = addItemPage.selectOkButton();

		invoicePage.clickToFabButton();

		addCustomerPage = (AddCustomerPageObject) invoicePage.clickToAddCustomerButton();
		
		addCustomerPage.sendkeysToFirstName("Hung");
		
		addCustomerPage.sendkeysToLastName("Nguyen");
		
		addCustomerPage.sendkeysToEmail("hung.nguyen@enouvo.com");
		
		addCustomerPage.sendkeysToPhone("0905495922");
		
		addCustomerPage.sendkeysToCustomerBillingName("Hung Nguyen");
		
		addCustomerPage.sendkeysToGSTNumber("33AORPP2933E1ZV");
		
		addCustomerPage.moveToElement(driver, "SHIPPING ADDRESS");
		
		addCustomerPage.sendkeysToStreetAddress("15 Ta My Duat");
		
		addCustomerPage.sendkeysToCity("Da Nang");
		
		addCustomerPage.clickToState();
		
		addCustomerPage.selectDynamicState("Karnataka");
		
		addCustomerPage.sendkeysToPostCode("550000");
		
		addCustomerPage.sendkeysToCountry("Viet Nam");
		
		addCustomerPage.clickToUseSameAsBilling();
		
		addCustomerPage.submitTransaction();
		
		invoicePage = addCustomerPage.clickToOkButton();
		
		log.info("Add new invoice: Sent Invoice");
		
		invoicePage.clickToFabButton();

		addInvoicePage = invoicePage.clickToAddInvoiceButton();
		
		addInvoicePage.clickToAddCustomerButton();
		
		addInvoicePage.selectFirstItemCustomer();
		
		addInvoicePage.clickToAddItemButton();
		
		addInvoicePage.selectFirstItemCustomer();
		
		addInvoicePage.selectSubmitButton();
		
		addInvoicePage.selectStatusCategory();
		
		addInvoicePage.selectDynamicStatus("Sent");
		
		addInvoicePage.selectDynamicButton("Save");
		
		addInvoicePage.clickOkButton();
		
	}

	@AfterTest(alwaysRun = true)
	public void quitBrowser() {
		service.stop();
	}

}
