package myaccountant.account;

import org.junit.Before;
import org.junit.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import static io.restassured.RestAssured.*;

public class restAPi {

	@Before
    public void init() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.basePath = "/student";
        RestAssured.port = 8080;
    }
 
    @Test
    public void getListStudent() {
        Response res = given()
                .when()
                .get("/list");
 
        System.out.println(res.prettyPeek());
        res.then().statusCode(200);
 
    }
}
