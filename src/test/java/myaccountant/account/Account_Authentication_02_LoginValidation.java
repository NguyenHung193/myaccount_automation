package myaccountant.account;

import static commons.AbstractTest.service;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import commons.AbstractTest;
import commons.PageFactoryManage;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import myaccountant.pageObjects.AddIncomeExpensePaymentPageObject;
import myaccountant.pageObjects.AuthenticationPageObject;
import myaccountant.pageObjects.DashboardPageObject;
import myaccountant.pageObjects.LoginPageObject;

public class Account_Authentication_02_LoginValidation extends AbstractTest {

	private AndroidDriver<AndroidElement> driver;
	AuthenticationPageObject authenticationPage;
	LoginPageObject loginPage;
	DashboardPageObject dashboardPage;
	AddIncomeExpensePaymentPageObject addIncomeExpensePaymentPage;

	String correctEmail, incorrectEmail, incorrectFormatEmail, correctPassword, incorrectPassword;

	@BeforeTest
	public void beforeTest() {

		correctEmail = "hung.nguyen@enouvo.com";
		incorrectEmail = "hungnguyen@enouvo.com";
		correctPassword = "123456";
		incorrectPassword = "12345678";
		incorrectFormatEmail = "hung.nguyen";

		killAllNodes();

		service = startServer();

		driver = openAndroidDevice("MyAccountantStaging");

		authenticationPage = PageFactoryManage.getAuthenticationPage(driver);
	}

	@Test
	public void TC_01_SendEmptyData() {

		log.info("LoginPage: SendEmptyData");

		loginPage = authenticationPage.clickToLoginButton();

		loginPage.sendkeyToDynamicInput("", "EMAIL");

		loginPage.sendkeyToDynamicInput("", "PASSWORD");

		loginPage.clickToLoginButtonNotValid();

		verifyTrue(loginPage.isEmailEmptyMessageDisplayed());

		loginPage.clickToOkButton();
//		dashboardPage = loginPage.clickToLoginButton();
	}

	@Test
	public void TC_02_SendEmptyEmail() {

		log.info("LoginPage: SendEmptyEmail");

		authenticationPage = loginPage.clickToCloseIcon();

		loginPage = authenticationPage.clickToLoginButton();

		loginPage.sendkeyToDynamicInput("", "EMAIL");

		loginPage.sendkeyToDynamicInput(correctPassword, "PASSWORD");

		loginPage.clickToLoginButtonNotValid();

		verifyTrue(loginPage.isEmailEmptyMessageDisplayed());

		loginPage.clickToOkButton();
	}

	@Test
	public void TC_03_SendIncorrectFormatEmail() {

		log.info("LoginPage: SendIncorrectFormatEmail");

		authenticationPage = loginPage.clickToCloseIcon();

		loginPage = authenticationPage.clickToLoginButton();

		loginPage.sendkeyToDynamicInput(incorrectFormatEmail, "EMAIL");

		loginPage.sendkeyToDynamicInput(correctPassword, "PASSWORD");

		loginPage.clickToLoginButtonNotValid();

		verifyTrue(loginPage.isEmailNotCorrectMessageDisplayed());

		loginPage.clickToOkButton();
	}

	@Test
	public void TC_04_SendIncorrectEmail() {

		log.info("LoginPage: SendIncorrectEmail");
		
		authenticationPage = loginPage.clickToCloseIcon();

		loginPage = authenticationPage.clickToLoginButton();

		loginPage.sendkeyToDynamicInput(incorrectEmail, "EMAIL");

		loginPage.sendkeyToDynamicInput(correctPassword, "PASSWORD");

		loginPage.clickToLoginButtonNotValid();

		verifyTrue(loginPage.isEmailNotFoundMessageDisplayed());

		loginPage.clickToOkButton();
	}

	@Test
	public void TC_05_SendIncorrectPassword() {

		log.info("LoginPage: SendIncorrectPassword");
		
		authenticationPage = loginPage.clickToCloseIcon();

		loginPage = authenticationPage.clickToLoginButton();

		loginPage.sendkeyToDynamicInput(correctEmail, "EMAIL");

		loginPage.sendkeyToDynamicInput(incorrectPassword, "PASSWORD");

		loginPage.clickToLoginButtonNotValid();

		verifyTrue(loginPage.isPasswordInCorrectMessageDisplayed());

		loginPage.clickToOkButton();
	}

	@Test
	public void TC_06_SendEmptyPassword() {

		log.info("LoginPage: SendEmptyPassword");
		
		authenticationPage = loginPage.clickToCloseIcon();

		loginPage = authenticationPage.clickToLoginButton();

		loginPage.sendkeyToDynamicInput(correctEmail, "EMAIL");

		loginPage.sendkeyToDynamicInput("", "PASSWORD");

		loginPage.clickToLoginButtonNotValid();

		verifyTrue(loginPage.isPasswordEmptyMessageDisplayed());

		loginPage.clickToOkButton();
	}

	@Test
	public void TC_07_SendPasswordLessThan6Digit() {

		log.info("LoginPage: SendPasswordLessThan6Digit");
		
		authenticationPage = loginPage.clickToCloseIcon();

		loginPage = authenticationPage.clickToLoginButton();

		loginPage.sendkeyToDynamicInput(correctEmail, "EMAIL");

		loginPage.sendkeyToDynamicInput("123", "PASSWORD");

		loginPage.clickToLoginButtonNotValid();

		verifyTrue(loginPage.isPasswordLessThan6DigitMessageDisplayed());

		loginPage.clickToOkButton();
	}

//	@Test
//	public void TC_02_Dashboard() {
//
//		dashboardPage.clickToFabButton();
//
//		addIncomeExpensePaymentPage = dashboardPage.clickToAddIncomeButton();
//		
//		addIncomeExpensePaymentPage.selectCash();
//		
//		addIncomeExpensePaymentPage.selectCategory();
//		
//		addIncomeExpensePaymentPage.selectCategoryValue("Bank Interest");
//		
//		addIncomeExpensePaymentPage.sendkeysToAmount("1000");
//		
//		addIncomeExpensePaymentPage.submitTransaction();
//		
//		addIncomeExpensePaymentPage.selectOkButton();
//		
////		dashboardPage.sleepInSecond(90000);
//
////		dashboardPage.clickToMenuButton(driver);
////		
////		dashboardPage.clickToDynamicMenuLink(driver, SwipeBarMenuData.REPORTS);
////		
////		dashboardPage.moveToElement(driver, ReportPageData.NET_ASSERTS);
//	}

	@SuppressWarnings("rawtypes")
	@AfterTest(alwaysRun = true)
	public void quitBrowser() {
		service.stop();
//		((AppiumDriver)driver).closeApp();
	}

}
