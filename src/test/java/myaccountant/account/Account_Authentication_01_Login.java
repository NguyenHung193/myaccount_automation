package myaccountant.account;

import static commons.AbstractTest.service;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import commons.AbstractTest;
import commons.PageFactoryManage;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import myaccountant.pageObjects.AddIncomeExpensePaymentPageObject;
import myaccountant.pageObjects.AuthenticationPageObject;
import myaccountant.pageObjects.DashboardPageObject;
import myaccountant.pageObjects.LoginPageObject;
import myaccountant.pageObjects.ReportPageObject;
import testData.SwipeBarMenuData;

public class Account_Authentication_01_Login extends AbstractTest {

	private AndroidDriver<AndroidElement> driver;
	AuthenticationPageObject authenticationPage;
	LoginPageObject loginPage;
	DashboardPageObject dashboardPage;
	AddIncomeExpensePaymentPageObject addIncomeExpensePaymentPage;
	ReportPageObject reportPage;

	@BeforeTest
	public void beforeTest() {

		killAllNodes();

		service = startServer();

		driver = openAndroidDevice("MyAccountantStaging");

		authenticationPage = PageFactoryManage.getAuthenticationPage(driver);
	}

	@Test
	public void TC_01_LoginToSystem() {

		loginPage = authenticationPage.clickToLoginButton();

		loginPage.sendkeyToDynamicInput("hung.nguyen@enouvo.com", "EMAIL");

		loginPage.sendkeyToDynamicInput("123456", "PASSWORD");

		dashboardPage = loginPage.clickToLoginButton();
		
		dashboardPage.clickToMenuButton(driver);
		
		reportPage = (ReportPageObject) dashboardPage.clickToDynamicMenuLink(driver, SwipeBarMenuData.REPORTS);
		
		reportPage.isTotalIncomeDisplayed();

	}

//	@Test
//	public void TC_02_Dashboard() {
//
//		dashboardPage.clickToFabButton();
//
//		addIncomeExpensePaymentPage = dashboardPage.clickToAddIncomeButton();
//		
//		addIncomeExpensePaymentPage.selectCash();
//		
//		addIncomeExpensePaymentPage.selectCategory();
//		
//		addIncomeExpensePaymentPage.selectCategoryValue("Bank Interest");
//		
//		addIncomeExpensePaymentPage.sendkeysToAmount("1000");
//		
//		addIncomeExpensePaymentPage.submitTransaction();
//		
//		addIncomeExpensePaymentPage.selectOkButton();
//		
////		dashboardPage.sleepInSecond(90000);
//
////		dashboardPage.clickToMenuButton(driver);
////		
////		dashboardPage.clickToDynamicMenuLink(driver, SwipeBarMenuData.REPORTS);
////		
////		dashboardPage.moveToElement(driver, ReportPageData.NET_ASSERTS);
//	}

	@AfterTest(alwaysRun = true)
	public void quitBrowser() {
		service.stop();
	}

}
