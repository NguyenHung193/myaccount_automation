package myaccountant.account;

import static commons.AbstractTest.service;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import commons.AbstractTest;
import commons.PageFactoryManage;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import myaccountant.pageObjects.AddIncomeExpensePaymentPageObject;
import myaccountant.pageObjects.AuthenticationPageObject;
import myaccountant.pageObjects.DashboardPageObject;
import myaccountant.pageObjects.LoginPageObject;
import myaccountant.pageObjects.MyAccountantPageObject;
import myaccountant.pageObjects.MyClientPageObject;
import myaccountant.pageObjects.ReportPageObject;
import myaccountant.pageObjects.SettingsPageObject;
import myaccountant.pageObjects.SignUpAsAccountPageObject;
import myaccountant.pageObjects.SignUpPageObject;
import testData.ReportPageData;
import testData.SwipeBarMenuData;

public class Account_Authentication_04_SignUpAsAccountant extends AbstractTest {

	private AndroidDriver<AndroidElement> driver;
	AuthenticationPageObject authenticationPage;
	SignUpAsAccountPageObject signUpAsAccountantPage;
	DashboardPageObject dashboardPage;
	AddIncomeExpensePaymentPageObject addIncomeExpensePaymentPage;
	ReportPageObject reportPage;
	SettingsPageObject settingsPage;
	SignUpPageObject signUpPage;
	MyAccountantPageObject myaccountantPage;
	LoginPageObject loginPage;
	MyClientPageObject myClientsPage;

	String emailClient = "hung.nguyen.enouvotest+" + "client" + randomNumber() + "@gmail.com";
	String emailAccountant = "hung.nguyen.enouvotest+" + "AC" + randomNumber() + "@gmail.com";
	String firstName, lastName, phoneNumber, password, confirmPassword, verifyCode;
	String incomeCash, expenseCash, incomeCredit, expenseCredit, paymentMountIn, paymentMountOut;
	String businessName, streetAddress, city, dist, state, zipCode;

	@BeforeTest
	public void beforeTest() {

		incomeCash = "1000";
		expenseCash = "2000";
		incomeCredit = "500";
		expenseCredit = "300";
		paymentMountIn = "600";
		paymentMountOut = "800";

		killAllNodes();

		service = startServer();

		driver = openAndroidDevice("MyAccountantStaging");

		authenticationPage = PageFactoryManage.getAuthenticationPage(driver);

		firstName = "Hung";
		lastName = "Nguyen";
		phoneNumber = "0905495922";
		password = "Hung0905495922";
		confirmPassword = "Hung0905495922";
		businessName = "Enouvo";
		streetAddress = "15 Ta My Duat";
		city = "Da Nang";
		dist = "Hai Chau";
		state = "Da Nang";
		zipCode = "55000";

	}

	@Test
	public void TC_01_LoginToSystemAsAccountant() {

		log.info("Steps 1: Creating new accountant");

		signUpAsAccountantPage = authenticationPage.clickToSignUpAsAccountButton();

		signUpAsAccountantPage.sendkeyToDynamicSignUpInput(firstName, "First name");

		signUpAsAccountantPage.sendkeyToDynamicSignUpInput(lastName, "Last name");

		signUpAsAccountantPage.sendkeyToDynamicSignUpInput(emailAccountant, "Email");

		signUpAsAccountantPage.sendkeyToDynamicSignUpInput(phoneNumber, "Phone number");

		signUpAsAccountantPage.sendkeyToDynamicSignUpInput(password, "Password");

		signUpAsAccountantPage.sendkeyToDynamicSignUpInput(confirmPassword, "Confirm Password");

		signUpAsAccountantPage.clickToNextButton();
		
		log.info("Steps 2: Entry accountant info");

		signUpAsAccountantPage.sendkeyToDynamicSignUpInput(businessName, "Business name");

		signUpAsAccountantPage.sendkeyToDynamicSignUpInput(streetAddress, "Street address");

		signUpAsAccountantPage.sendkeyToDynamicSignUpInput(city, "City");

		signUpAsAccountantPage.sendkeyToDynamicSignUpInput(dist, "Dist");

		signUpAsAccountantPage.sendkeyToDynamicSignUpInput(dist, "State");

		signUpAsAccountantPage.sendkeyToDynamicSignUpInput(zipCode, "Zip code");

		signUpAsAccountantPage.clickToSignUpButton();
		
		log.info("Steps 3: Verifying email code");

		signUpAsAccountantPage.waitForMailWasSent();

		String str = "<span style=\"font-size: 22pt\">";
		String a = verifyMail("hung.nguyen.enouvotest@gmail.com", "Hung0905495922", "MyAccountant Email Verification");
		String b = a.substring(a.indexOf(str) + str.length(),
				a.indexOf("<span style=\"font-size: 22pt\">") + 6 + str.length());
		System.out.println("VerifyCode: " + b);
		verifyCode = b;

		signUpAsAccountantPage.sendkeyToDynamicSignUpInput(verifyCode, "Verify code");

		signUpAsAccountantPage.clickToVerifynButton();

		dashboardPage = signUpAsAccountantPage.clickToSkipItButton();
		
		log.info("Steps 4: Logout");

		dashboardPage.clickToMenuButton(driver);

		settingsPage = (SettingsPageObject) dashboardPage.clickToDynamicMenuLink(driver, "Settings");

		settingsPage.sendkeyToDynamicMenuSettingsLink("Log Out");

		settingsPage.backToConfirmLogout();
	}

	@Test
	public void TC_02_LoginToSystemAsClient() {

		log.info("Steps 1: Creating new client account");

		signUpPage = authenticationPage.clickToSignUpButton();

		signUpPage.sendkeyToDynamicSignUpInput(firstName, "First name");

		signUpPage.sendkeyToDynamicSignUpInput(lastName, "Last name");

		signUpPage.sendkeyToDynamicSignUpInput(emailClient, "Email");

		signUpPage.sendkeyToDynamicSignUpInput(phoneNumber, "Phone number");

		signUpPage.sendkeyToDynamicSignUpInput(password, "Password");

		signUpPage.sendkeyToDynamicSignUpInput(confirmPassword, "Confirm Password");

		signUpPage.clickToNextButton();
		
		log.info("Steps 2: Verifying email code");

		signUpPage.waitForMailWasSent();

		String str = "<span style=\"font-size: 22pt\">";
		String a = verifyMail("hung.nguyen.enouvotest@gmail.com", "Hung0905495922", "MyAccountant Email Verification");
		String b = a.substring(a.indexOf(str) + str.length(),
				a.indexOf("<span style=\"font-size: 22pt\">") + 6 + str.length());
		System.out.println("VerifyCode: " + b);
		verifyCode = b;

		signUpPage.sendkeyToDynamicSignUpInput(verifyCode, "Verify code");

		signUpPage.clickToVerifynButton();

		dashboardPage = signUpPage.clickToSkipItButton();
		
		log.info("Steps 3: Open My accountant page");

		dashboardPage.clickToMenuButton(driver);

		myaccountantPage = (MyAccountantPageObject) dashboardPage.clickToDynamicMenuLink(driver, "My Accountant");
		
		log.info("Steps 4: Invite new accountant");

		myaccountantPage.clickAddMyAccountantButton();

		myaccountantPage.clickFirstAccountantButton();

		myaccountantPage.clickInviteButton();

		myaccountantPage.clickToOkButton();

		myaccountantPage.clickToOkButton();

		myaccountantPage.clickBackButton();

		myaccountantPage.clickToCloseListAccountantButton();
		
		log.info("Steps 5: Logout");

		myaccountantPage.clickToMenuButton(driver);

		settingsPage = (SettingsPageObject) dashboardPage.clickToDynamicMenuLink(driver, "Settings");

		settingsPage.sendkeyToDynamicMenuSettingsLink("Log Out");

		settingsPage.backToConfirmLogout();

	}

	@Test
	public void TC_03_ApproveClient() {
		
		log.info("Steps 1: Login accountant");

		loginPage = authenticationPage.clickToLoginButton();

		loginPage.sendkeyToDynamicInput(emailAccountant, "EMAIL");

		loginPage.sendkeyToDynamicInput(password, "PASSWORD");

		dashboardPage = loginPage.clickToLoginButton();
		
		log.info("Steps 2: Open My clients page");

		dashboardPage.clickToMenuButton(driver);

		myClientsPage = (MyClientPageObject) dashboardPage.clickToDynamicMenuLink(driver, "My Clients");
		
		log.info("Steps 3: Approve new client");

		myClientsPage.clickDynamicClientTab("NEW");

		myClientsPage.clickFirstClientButton();

		myClientsPage.clickApproveButton();

		myClientsPage.clickToOkButton();

		myClientsPage.clickToOkButton();

		myClientsPage.clickBackButton();

		myClientsPage.clickToMenuButton(driver);
		
		log.info("Steps 4: Logout");

		settingsPage = (SettingsPageObject) dashboardPage.clickToDynamicMenuLink(driver, "Settings");

		settingsPage.sendkeyToDynamicMenuSettingsLink("Log Out");

		settingsPage.backToConfirmLogout();

	}

	@Test
	public void TC_04_CofirmAccountant() {
		
		log.info("Steps 1: Login client");

		loginPage = authenticationPage.clickToLoginButton();

		loginPage.sendkeyToDynamicInput(emailClient, "EMAIL");

		loginPage.sendkeyToDynamicInput(password, "PASSWORD");

		dashboardPage = loginPage.clickToLoginButton();
		
		log.info("Steps 2: Open Myaccountant page");

		dashboardPage.clickToMenuButton(driver);

		myaccountantPage = (MyAccountantPageObject) dashboardPage.clickToDynamicMenuLink(driver, "My Accountant");
		
		log.info("Steps 3: Confirm accountant");

		myaccountantPage.clickFirstAccountantButton();

		myaccountantPage.clickConfirmButton();

		myaccountantPage.clickToOkButton();

		myaccountantPage.clickToOkButton();

		myaccountantPage.clickBackButton();

		myaccountantPage.clickToMenuButton(driver);

		log.info("Steps 4: Logout");
		
		settingsPage = (SettingsPageObject) dashboardPage.clickToDynamicMenuLink(driver, "Settings");

		settingsPage.sendkeyToDynamicMenuSettingsLink("Log Out");

		settingsPage.backToConfirmLogout();
	}

	@AfterTest(alwaysRun = true)
	public void quitBrowser() {
		service.stop();
	}

}
