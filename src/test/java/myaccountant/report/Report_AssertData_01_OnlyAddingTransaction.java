package myaccountant.report;

import static commons.AbstractTest.service;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import commons.AbstractTest;
import commons.PageFactoryManage;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import myaccountant.pageObjects.AddIncomeExpensePaymentPageObject;
import myaccountant.pageObjects.AuthenticationPageObject;
import myaccountant.pageObjects.DashboardPageObject;
import myaccountant.pageObjects.LoginPageObject;
import myaccountant.pageObjects.ReportPageObject;
import myaccountant.pageObjects.SignUpPageObject;
import testData.ReportPageData;
import testData.SwipeBarMenuData;

public class Report_AssertData_01_OnlyAddingTransaction extends AbstractTest {

	private AndroidDriver<AndroidElement> driver;
	AuthenticationPageObject authenticationPage;
	SignUpPageObject signUpPage;
	DashboardPageObject dashboardPage;
	AddIncomeExpensePaymentPageObject addIncomeExpensePaymentPage;
	ReportPageObject reportPage;

	String email = "hung.nguyen.enouvotest+" + "rp" +randomNumber() + "@gmail.com";
	String firstName, lastName, phoneNumber, password, confirmPassword, verifyCode;
	String incomeCash, expenseCash, incomeCredit, expenseCredit, paymentMountIn, paymentMountOut;

	@BeforeTest
	public void beforeTest() {

		incomeCash = "1000";
		expenseCash = "2000";
		incomeCredit = "500";
		expenseCredit = "300";
		paymentMountIn = "600";
		paymentMountOut = "800";

		killAllNodes();

		service = startServer();

		driver = openAndroidDevice("MyAccountantStaging");

		authenticationPage = PageFactoryManage.getAuthenticationPage(driver);

		firstName = "Hung";
		lastName = "Nguyen";
		phoneNumber = "0905495922";
		password = "Hung0905495922";
		confirmPassword = "Hung0905495922";

	}

	@Test
	public void TC_01_LoginToSystem() {

		log.info("LoginPage: Creating new account and login");

		signUpPage = authenticationPage.clickToSignUpButton();

		signUpPage.sendkeyToDynamicSignUpInput(firstName, "First name");

		signUpPage.sendkeyToDynamicSignUpInput(lastName, "Last name");

		signUpPage.sendkeyToDynamicSignUpInput(email, "Email");

		signUpPage.sendkeyToDynamicSignUpInput(phoneNumber, "Phone number");

		signUpPage.sendkeyToDynamicSignUpInput(password, "Password");

		signUpPage.sendkeyToDynamicSignUpInput(confirmPassword, "Confirm Password");

		signUpPage.clickToNextButton();

		signUpPage.waitForMailWasSent();

		String str = "<span style=\"font-size: 22pt\">";
		String a = verifyMail("hung.nguyen.enouvotest@gmail.com", "Hung0905495922", "MyAccountant Email Verification");
		String b = a.substring(a.indexOf(str) + str.length(),
				a.indexOf("<span style=\"font-size: 22pt\">") + 6 + str.length());
		System.out.println("VerifyCode: " + b);
		verifyCode = b;

		signUpPage.sendkeyToDynamicSignUpInput(verifyCode, "Verify code");

		signUpPage.clickToVerifynButton();

		dashboardPage = signUpPage.clickToSkipItButton();
	}

	@Test
	public void TC_02_EntryIncomeCashTransaction() {

		log.info("Add new transaction: IncomeCashTransaction");

		dashboardPage.clickToFabButton();

		addIncomeExpensePaymentPage = dashboardPage.clickToAddIncomeButton();

		addIncomeExpensePaymentPage.selectCash();

		addIncomeExpensePaymentPage.selectCategory();

		addIncomeExpensePaymentPage.selectCategoryValue("Bank Interest");

		addIncomeExpensePaymentPage.sendkeysToAmount(incomeCash);

		addIncomeExpensePaymentPage.submitTransaction();

		addIncomeExpensePaymentPage.selectOkButton();
	}

	@Test
	public void TC_03_EntryExpenseCashTransaction() {

		log.info("Add new transaction: ExpenseCashTransaction");

		dashboardPage.clickToFabButton();

		addIncomeExpensePaymentPage = dashboardPage.clickToAddExpenseButton();

		addIncomeExpensePaymentPage.selectCash();

		addIncomeExpensePaymentPage.selectCategory();

		addIncomeExpensePaymentPage.selectCategoryValue("Bank Interest");

		addIncomeExpensePaymentPage.sendkeysToAmount(expenseCash);

		addIncomeExpensePaymentPage.submitTransaction();

		addIncomeExpensePaymentPage.selectOkButton();
	}

	@Test
	public void TC_04_EntryPaymentMoneyInTransaction() {

		log.info("Add new transaction: PaymentMoneyInTransaction");

		dashboardPage.clickToFabButton();

		addIncomeExpensePaymentPage = dashboardPage.clickToAddPaymentButton();

		addIncomeExpensePaymentPage.selectMoneyIn();

		addIncomeExpensePaymentPage.selectCategory();

		addIncomeExpensePaymentPage.selectCategoryValue("Accounts Receivable");

		addIncomeExpensePaymentPage.sendkeysToAmount(paymentMountIn);

		addIncomeExpensePaymentPage.submitTransaction();

		addIncomeExpensePaymentPage.selectOkButton();
	}

	@Test
	public void TC_05_EntryIncomeCreditTransaction() {

		log.info("Add new transaction: IncomeCreditTransaction");

		dashboardPage.clickToFabButton();

		addIncomeExpensePaymentPage = dashboardPage.clickToAddIncomeButton();

		addIncomeExpensePaymentPage.selectCredit();

		addIncomeExpensePaymentPage.selectCategory();

		addIncomeExpensePaymentPage.selectCategoryValue("Commission");

		addIncomeExpensePaymentPage.sendkeysToAmount(incomeCredit);

		addIncomeExpensePaymentPage.submitTransaction();

		addIncomeExpensePaymentPage.selectOkButton();
	}

	@Test
	public void TC_06_EntryExpenseCreditTransaction() {

		log.info("Add new transaction: ExpenseCreditTransaction");

		dashboardPage.clickToFabButton();

		addIncomeExpensePaymentPage = dashboardPage.clickToAddExpenseButton();

		addIncomeExpensePaymentPage.selectCredit();

		addIncomeExpensePaymentPage.selectCategory();

		addIncomeExpensePaymentPage.selectCategoryValue("Accountant Fees");

		addIncomeExpensePaymentPage.sendkeysToAmount(expenseCredit);

		addIncomeExpensePaymentPage.submitTransaction();

		addIncomeExpensePaymentPage.selectOkButton();
	}

	@Test
	public void TC_07_EntryPaymentMoneyOutTransaction() {

		log.info("Add new transaction: PaymentMoneyOutTransaction");

		dashboardPage.clickToFabButton();

		addIncomeExpensePaymentPage = dashboardPage.clickToAddPaymentButton();

		addIncomeExpensePaymentPage.selectMoneyOut();

		addIncomeExpensePaymentPage.selectCategory();

		addIncomeExpensePaymentPage.selectCategoryValue("Accounts Receivable");

		addIncomeExpensePaymentPage.sendkeysToAmount(paymentMountOut);

		addIncomeExpensePaymentPage.submitTransaction();

		addIncomeExpensePaymentPage.selectOkButton();

		dashboardPage.clickToMenuButton(driver);

		reportPage = (ReportPageObject) dashboardPage.clickToDynamicMenuLink(driver, SwipeBarMenuData.REPORTS);

//		reportPage.moveToNetAssert();
	}

	@Test
	public void TC_08_AssertReport() {

		log.info("Verifying Report: Total Income");

		reportPage.moveToDynamicElement(ReportPageData.TOTAL_INCOME);

		verifyTrue(reportPage.getDynamicValueNet_Profit_2_ReportValue(ReportPageData.TOTAL_INCOME) ==
				Math.abs(Double.valueOf(incomeCash) + Double.valueOf(incomeCredit)));
		
		log.info("Verifying Report: Total Expense");
		
		reportPage.moveToDynamicElement(ReportPageData.TOTAL_EXPENSE);

		verifyTrue(reportPage.getDynamicValueNet_Profit_2_ReportValue(ReportPageData.TOTAL_EXPENSE) ==
				Math.abs(Double.valueOf(expenseCash) + Double.valueOf(expenseCredit)));
		
		log.info("Verifying Report: Net");

		reportPage.moveToDynamicElement(ReportPageData.NET);

		verifyTrue(reportPage.getDynamicValueNet_Profit_2_ReportValue(ReportPageData.NET) == Math.abs(Double.valueOf(incomeCash)
				+ Double.valueOf(incomeCredit) - Double.valueOf(expenseCash) - Double.valueOf(expenseCredit)));
		
		log.info("Verifying Report: Cash");

		reportPage.moveToDynamicElement(ReportPageData.CASH);

		verifyTrue(reportPage.getDynamicValueNet_Profit_2_ReportValue(ReportPageData.CASH) == Math.abs(Double.valueOf(incomeCash)
				- Double.valueOf(expenseCash) + Double.valueOf(paymentMountIn) - Double.valueOf(paymentMountOut)));
		
		log.info("Verifying Report: Accounts Receivable");

		reportPage.moveToDynamicElement(ReportPageData.ACCOUNTS_RECEIVABLE);

		verifyTrue(reportPage.getDynamicValueNet_Profit_2_ReportValue(ReportPageData.ACCOUNTS_RECEIVABLE) == Math.abs(Double.valueOf(incomeCredit)
				+ Double.valueOf(paymentMountOut) - Double.valueOf(paymentMountIn)));

	}

	@AfterTest(alwaysRun = true)
	public void quitBrowser() {
		service.stop();
	}

}
