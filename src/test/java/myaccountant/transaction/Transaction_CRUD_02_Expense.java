package myaccountant.transaction;

import static commons.AbstractTest.service;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import commons.AbstractTest;
import commons.PageFactoryManage;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import myaccountant.pageObjects.ActivityPageObject;
import myaccountant.pageObjects.AddIncomeExpensePaymentPageObject;
import myaccountant.pageObjects.AuthenticationPageObject;
import myaccountant.pageObjects.DashboardPageObject;
import myaccountant.pageObjects.EditTransactionPageObject;
import myaccountant.pageObjects.LoginPageObject;
import myaccountant.pageObjects.ReportPageObject;
import myaccountant.pageObjects.SignUpPageObject;
import testData.ReportPageData;
import testData.SwipeBarMenuData;

public class Transaction_CRUD_02_Expense extends AbstractTest {

	private AndroidDriver<AndroidElement> driver;
	AuthenticationPageObject authenticationPage;
	SignUpPageObject signUpPage;
	DashboardPageObject dashboardPage;
	AddIncomeExpensePaymentPageObject addIncomeExpensePaymentPage;
	ReportPageObject reportPage;
	ActivityPageObject activityPage;
	EditTransactionPageObject editTransactionPage;

	String email = "hung.nguyen.enouvotest+" + "expense" + randomNumber() + "@gmail.com";
	String firstName, lastName, phoneNumber, password, confirmPassword, verifyCode;
	String incomeCash, expenseCash, incomeCredit, expenseCredit, paymentMountIn, paymentMountOut;
	String newAmount;

	@BeforeTest
	public void beforeTest() {

		incomeCash = "1000";
		expenseCash = "2000";
		incomeCredit = "500";
		expenseCredit = "300";
		paymentMountIn = "600";
		paymentMountOut = "800";

		killAllNodes();

		service = startServer();

		driver = openAndroidDevice("MyAccountantStaging");

		authenticationPage = PageFactoryManage.getAuthenticationPage(driver);

		firstName = "Hung";
		lastName = "Nguyen";
		phoneNumber = "0905495922";
		password = "Hung0905495922";
		confirmPassword = "Hung0905495922";

	}

	@Test
	public void TC_01_LoginToSystem() {
		
		log.info("+++++++++++++++++++email"+ email);

		log.info("LoginPage: Creating new account and login");

		signUpPage = authenticationPage.clickToSignUpButton();

		signUpPage.sendkeyToDynamicSignUpInput(firstName, "First name");

		signUpPage.sendkeyToDynamicSignUpInput(lastName, "Last name");

		signUpPage.sendkeyToDynamicSignUpInput(email, "Email");

		signUpPage.sendkeyToDynamicSignUpInput(phoneNumber, "Phone number");

		signUpPage.sendkeyToDynamicSignUpInput(password, "Password");

		signUpPage.sendkeyToDynamicSignUpInput(confirmPassword, "Confirm Password");

		signUpPage.clickToNextButton();

		signUpPage.waitForMailWasSent();

		String str = "<span style=\"font-size: 22pt\">";
		String a = verifyMail("hung.nguyen.enouvotest@gmail.com", "Hung0905495922", "MyAccountant Email Verification");
		String b = a.substring(a.indexOf(str) + str.length(),
				a.indexOf("<span style=\"font-size: 22pt\">") + 6 + str.length());
		System.out.println("VerifyCode: " + b);
		verifyCode = b;

		signUpPage.sendkeyToDynamicSignUpInput(verifyCode, "Verify code");

		signUpPage.clickToVerifynButton();

		dashboardPage = signUpPage.clickToSkipItButton();
	}

	@Test
	public void TC_02_EntryExpenseCashTransaction() {

		log.info("Add new transaction: ExpenseCashTransaction");

		dashboardPage.clickToFabButton();

		addIncomeExpensePaymentPage = dashboardPage.clickToAddExpenseButton();

		addIncomeExpensePaymentPage.selectCash();

		addIncomeExpensePaymentPage.selectCategory();

		addIncomeExpensePaymentPage.selectCategoryValue("Accountant Fees");

		addIncomeExpensePaymentPage.sendkeysToAmount(expenseCash);

		addIncomeExpensePaymentPage.submitTransaction();

		addIncomeExpensePaymentPage.selectOkButton();
	}
	
	@Test
	public void TC_03_EditExpenseCashTransaction() {
		
		log.info("Edit transaction: ExpenseCashTransaction");
		
		dashboardPage.clickToMenuButton(driver);

		activityPage = (ActivityPageObject) dashboardPage.clickToDynamicMenuLink(driver, SwipeBarMenuData.ACTIVITY);
		
		activityPage.clickToDynamicTab("EXPENSE");
		
		editTransactionPage = activityPage.clickToDynamicTransaction("2");
		
		newAmount= "2000";
		editTransactionPage.sendkeysToAmount(newAmount);
		
		editTransactionPage.submitTransaction();
		
		activityPage = editTransactionPage.selectOkButton();
	}

	@Test
	public void TC_03_EntryExpenseCreditTransaction() {

		log.info("Add new transaction: ExpenseCreditTransaction");

		activityPage.clickToFabButton();

		addIncomeExpensePaymentPage = dashboardPage.clickToAddExpenseButton();

		addIncomeExpensePaymentPage.selectCredit();

		addIncomeExpensePaymentPage.selectCategory();

		addIncomeExpensePaymentPage.selectCategoryValue("Bank Interest");

		addIncomeExpensePaymentPage.sendkeysToAmount(expenseCredit);

		addIncomeExpensePaymentPage.submitTransaction();

		addIncomeExpensePaymentPage.selectOkButton();
	}

	@Test
	public void TC_04_EditExpenseCashTransaction() {
		
		log.info("Edit transaction: IncomeCreditTransaction");
		
		activityPage.clickToDynamicTab("EXPENSE");
		
		editTransactionPage = activityPage.clickToDynamicTransaction("2");
		
		newAmount= "3000";
		editTransactionPage.sendkeysToAmount(newAmount);
		
		editTransactionPage.submitTransaction();
		
		activityPage = editTransactionPage.selectOkButton();
	}
	
	@Test
	public void TC_05_DeleteExpenseTransaction() {
		
		log.info("Edit transaction: IncomeCreditTransaction");
		
		activityPage.clickToDynamicTab("EXPENSE");
		
		editTransactionPage = activityPage.clickToDynamicTransaction("2");
		
		editTransactionPage.deleteTransaction();
		
		editTransactionPage.confirmDeleteTransaction();
		
		activityPage = editTransactionPage.selectOkButton();
	}

	@AfterTest(alwaysRun = true)
	public void quitBrowser() {
		service.stop();
	}

}
