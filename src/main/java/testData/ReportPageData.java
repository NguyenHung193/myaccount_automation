package testData;

public class ReportPageData {

	public static final String NET_ASSERT = "NET ASSET";
	
	public static final String INCOME = "INCOME";
	public static final String BANK_INTEREST = "Bank Interest";
	public static final String TOTAL_INCOME = "Total Income";
	public static final String EXPENSES = "EXPENSES";
	public static final String TOTAL_EXPENSE = "Total Expense";
	public static final String NET = "Net";
	public static final String ASSETS = "ASSETS";
	public static final String CASH = "Cash";
	public static final String ACCOUNTS_RECEIVABLE = "Accounts Receivable";
	public static final String TOTAL_ASSETS = "Total Assets";
	public static final String LIABILITIES = "LIABILITIES";
	public static final String ACCOUNTS_PAYABLE = "Accounts Payable";
	public static final String RETAINED_EARNINGS = "RETAINED EARNINGS";
//	public static final String NET_ASSERTS = "NET ASSET";
	
}
