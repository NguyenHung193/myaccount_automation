package myaccountantUI;

public class AddInvoicePageUI {

	public static final String ADD_CUSTOMER = "//android.widget.TextView[@text='ADD CUSTOMER']";
	public static final String ADD_ITEM = "//android.widget.TextView[@text='ADD ITEM']";
	public static final String DYNAMIC_BUTTON = "//android.widget.TextView[@text='%s']";
	public static final String STATUS_CATEGORY = "//android.widget.TextView[@text='Draft']";
	public static final String SELECT_DYNAMIC_STATUS = "//android.widget.TextView[@text='%s']";
	public static final String OK_BUTTON = "//android.widget.Button[@resource-id='android:id/button1']";
	public static final String SELECT_FIRST_ITEM_CUSTOMER = "//b.i.a.b//android.view.ViewGroup//android.view.ViewGroup//android.widget.FrameLayout//android.widget.ScrollView//android.view.ViewGroup[@index='1']//android.view.ViewGroup";
	public static final String SUBMIT_BUTTON = "//androidx.appcompat.widget.LinearLayoutCompat//android.widget.TextView[@index='0']";
}

