package myaccountantUI;

public class AddCustomerPageUI {

	public static final String FIRST_NAME = "//b.i.a.b//android.view.ViewGroup//android.view.ViewGroup//android.widget.FrameLayout//android.widget.ScrollView//android.widget.ScrollView//android.view.ViewGroup//android.view.ViewGroup[@index='1']//android.widget.EditText[@index='1']";
	public static final String LAST_NAME = "//android.widget.EditText[@text='Last name']";
	public static final String EMAIL = "//android.widget.EditText[@text='Email']";
	public static final String PHONE_NUMBER = "//android.widget.EditText[@text='Phone number']";
	public static final String CUSTOMER_BILLING_NAME = "//android.widget.EditText[@text='Customer billing name']";
	public static final String GST_NUMBER = "//android.widget.EditText[@text='GST NUMBER']";
	public static final String STREET_ADDRESS = "//android.widget.EditText[@text='Street address']";
	public static final String CITY = "//android.widget.EditText[@text='City']";
	public static final String STATE = "//android.widget.FrameLayout//b.i.a.b//android.view.ViewGroup//android.view.ViewGroup//android.widget.FrameLayout//android.view.ViewGroup//android.widget.ScrollView//android.view.ViewGroup//android.widget.ScrollView//android.view.ViewGroup//android.view.ViewGroup[@index='12']//android.view.ViewGroup//android.widget.EditText";
	public static final String POST_CODE = "//android.widget.EditText[@text='Post code']";
	public static final String COUNTRY = "//android.widget.EditText[@text='Country']";
	public static final String SELECT_DYNAMIC_STATE = "//android.widget.TextView[@text='%s']";
	
	public static final String USE_SAME_AS_BILLING_BUTTON = "//android.widget.Switch[@text='OFF']";
	public static final String SUBMIT_BUTTON = "//androidx.appcompat.widget.LinearLayoutCompat[@index='2']";
	public static final String SELECT_DYNAMIC_CATEGORY = "//android.widget.TextView[@text='%s']";
	
	public static final String OK_BUTTON = "//android.widget.Button[@resource-id='android:id/button1']";
}
