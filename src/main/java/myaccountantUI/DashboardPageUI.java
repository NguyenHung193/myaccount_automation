package myaccountantUI;

public class DashboardPageUI {

	public static final String NOTHANKS_POPUP = "//android.widget.Button[@text='NO, THANKS']";
	public static final String FAB_BUTTON = "//android.widget.TextView[@text='+']";
	public static final String MENU_BUTTON = "//android.widget.FrameLayout//android.widget.ImageButton";
	public static final String ADD_INCOME_BUTTON = "//android.widget.TextView[@text='Add Income']";
	public static final String ADD_EXPENSE_BUTTON = "//android.widget.TextView[@text='Add Expense']";
	public static final String ADD_PAYMENT_BUTTON = "//android.widget.TextView[@text='Add Payment']";
	
}
