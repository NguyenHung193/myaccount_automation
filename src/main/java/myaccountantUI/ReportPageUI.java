package myaccountantUI;

public class ReportPageUI {

	public static final String NET_ASSETS = "//android.widget.TextView[@text='NET ASSETS']";
	
	public static final String DYNAMIC_VALUE_NET_PROFIT_1 = "//android.widget.TextView[@text='%s']/following-sibling::android.widget.TextView[@index='2']";
	public static final String DYNAMIC_VALUE_NET_PROFIT_2 = "//android.widget.TextView[@text='%s']/following-sibling::android.widget.TextView[@index='3']";
	public static final String Income = "//android.widget.TextView[@text='Total Income']/following-sibling::android.widget.TextView[@index='3']";
}
