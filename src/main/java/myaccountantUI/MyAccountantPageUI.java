package myaccountantUI;

public class MyAccountantPageUI {

	public static final String OK_BUTTON = "//android.widget.Button[@resource-id='android:id/button1']";
	public static final String ADD_ACCOUNTANT_BUTTON = "//androidx.appcompat.widget.LinearLayoutCompat//android.widget.TextView";
	public static final String FIRST_ACCOUNTANT = "//android.widget.FrameLayout[@resource-id='android:id/content']//android.view.ViewGroup[@index='2']//android.view.ViewGroup//android.view.ViewGroup//android.widget.ScrollView//android.view.ViewGroup//android.view.ViewGroup[@index='0']//android.view.ViewGroup[@index='0']";
	public static final String INVITE_BUTTON = "//android.widget.TextView[@text='Invite']";
	public static final String CONFIRM_BUTTON = "//android.widget.TextView[@text='Confirm']";
	public static final String BACK_BUTTON = "//android.view.ViewGroup//android.widget.TextView[@text='g']";
	public static final String CLOSE_LIST_ACCOUNT_BUTTON = "//android.widget.FrameLayout[@resource-id='android:id/content']//android.view.ViewGroup[@index='2']//android.view.ViewGroup//android.widget.FrameLayout//android.view.ViewGroup//android.widget.ImageButton";
}
