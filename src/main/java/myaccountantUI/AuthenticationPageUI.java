package myaccountantUI;

public class AuthenticationPageUI {

	public static final String LOGIN_BUTTON = "//android.widget.TextView[@text='LOGIN']";
	public static final String SIGNUP_BUTTON = "//android.widget.TextView[@text='SIGN UP']";
	public static final String SIGNUP_AS_ACCOUNTANT_BUTTON = "//android.widget.TextView[@text='Sign up as accountant']";
}
