package commons;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

public class AbstractWebPage {
	WebElement element, elementDrag, elementDrop;
	JavascriptExecutor javascriptExecutor;
	WebDriverWait waitExplicit;
	List<WebElement> elements;
	Actions action;
	By byLocator;
	long shortTimeout = 5;
	long longTimeout = 30;

	public void openAnyUrl(WebDriver driver, String url) {
		driver.get(url);
	}

	public String getCurrentPageUrl(WebDriver driver) {
		return driver.getCurrentUrl();
	}

	public String getCurrentPageTitle(WebDriver driver) {
		return driver.getTitle();
	}

	public String getPageSource(WebDriver driver) {
		return driver.getPageSource();
	}

	public void backToPreviousPage(WebDriver driver) {
		driver.navigate().back();
	}

	public void refreshCurrentPage(WebDriver driver) {
		driver.navigate().refresh();
	}

	public void forward(WebDriver driver) {
		driver.navigate().forward();
	}

	public void acceptAlert(WebDriver driver) {
		driver.switchTo().alert().accept();
	}

	public void cancelAlert(WebDriver driver) {
		driver.switchTo().alert().dismiss();
	}

	public String getTextAlert(WebDriver driver) {
		return driver.switchTo().alert().getText();
	}

	public String getText(WebDriver driver, String locator) {
		element = driver.findElement(By.xpath(locator));
		return element.getText();
	}

	public String getText(WebDriver driver, String locator, String... values) {
		locator = String.format(locator, (Object[]) values);
		element = driver.findElement(By.xpath(locator));
		return element.getText();
	}

	public void clickToElement(WebDriver driver, String locator) {
//		highlightElement(driver, locator);
		element = driver.findElement(By.xpath(locator));
		element.click();
	}

	public void clickToElement(WebDriver driver, String locator, String... values) {
		locator = String.format(locator, (Object[]) values);
//		highlightElement(driver, locator);
		element = driver.findElement(By.xpath(locator));
		element.click();
	}

	public void clearElement(WebDriver driver, String locator) {
//		highlightElement(driver, locator);
		element = driver.findElement(By.xpath(locator));
		element.clear();
	}

	public void clearElement(WebDriver driver, String locator, String... values) {
		locator = String.format(locator, (Object[]) values);
//		highlightElement(driver, locator);
		element = driver.findElement(By.xpath(locator));
		element.clear();
	}

	public void sendkeyToElement(WebDriver driver, String locator, String valueToSendkey) {
//		highlightElement(driver, locator);
		element = driver.findElement(By.xpath(locator));
		element.sendKeys(valueToSendkey);
	}

	public void sendkeyToElement(WebDriver driver, String locator, String valueToSendkey, String... values) {
		locator = String.format(locator, (Object[]) values);
//		highlightElement(driver, locator);
		element = driver.findElement(By.xpath(locator));
		element.sendKeys(valueToSendkey);
	}

	public void selectItemDropdown(WebDriver driver, String locator, String value) {
		element = driver.findElement(By.xpath(locator));
		Select select = new Select(element);
		select.selectByVisibleText(value);
	}

	public String getSelectedItemInDropdown(WebDriver driver, String locator) {
		element = driver.findElement(By.xpath(locator));
		Select select = new Select(element);
		return select.getFirstSelectedOption().getText();
	}

	public void dropdownCustom(WebDriver driver, String dropdownIconXpath, String listElement, String expected)
			throws InterruptedException {
		WebElement dropdownIcon = driver.findElement(By.xpath(dropdownIconXpath));
		waitExplicit = new WebDriverWait(driver, 30);
		javascriptExecutor.executeScript("arguments[0].click();", dropdownIcon);
		// wait element

		waitExplicit.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(listElement)));

		List<WebElement> allElement = driver.findElements(By.xpath(listElement));

		System.out.println("Size" + allElement.size());

		for (int i = 0; i < allElement.size(); i++) {
			WebElement items = allElement.get(i);
			System.out.println("Item" + items);

			if (items.getText().contains(expected)) {

				if (items.isDisplayed()) {
					items.click();
				} else {
					javascriptExecutor.executeScript("arguments[0].scrollIntoView(true);", allElement.get(i));
					Thread.sleep(2000);
					javascriptExecutor.executeScript("arguments[0].click();", items);
					break;
				}

			}
		}

	}

	public String getAttributeValue(WebDriver driver, String locator, String attributeName) {
		element = driver.findElement(By.xpath(locator));
		return element.getAttribute(attributeName);
	}

	public int countElementNumber(WebDriver driver, String locator, String attributeName) {
		elements = driver.findElements(By.xpath(locator));
		return elements.size();
	}

	public void checkToCheckBox(WebDriver driver, String locator, String attributeName) {
		element = driver.findElement(By.xpath(locator));
		if (!element.isSelected()) {
			element.click();
		}
	}

	public void unCheckToCheckBox(WebDriver driver, String locator, String attributeName) {
		element = driver.findElement(By.xpath(locator));
		if (element.isSelected()) {
			element.click();
		}
	}

	public boolean isControlDisplayed(WebDriver driver, String locator) {
		boolean status = true;
		try {
			element = driver.findElement(By.xpath(locator));
			if (element.isDisplayed()) {
				return status;
			}
		} catch (Exception ex) {
			Reporter.log(
					"================================================================= Element not displayed ================================");
			Reporter.log(ex.getMessage());
			System.err.println(
					"================================================================= Element not displayed ================================");
			System.err.println(ex.getMessage());
			status = false;
		}
		return status;
	}

	public boolean isControlDisplayed(WebDriver driver, String locator, String... values) {
		boolean status = true;
		try {
			locator = String.format(locator, (Object[]) values);
			element = driver.findElement(By.xpath(locator));
			if (element.isDisplayed()) {
				return status;
			}
		} catch (Exception ex) {
			Reporter.log(
					"================================================================= Element not displayed ================================");
			Reporter.log(ex.getMessage());
			System.err.println(
					"================================================================= Element not displayed ================================");
			System.err.println(ex.getMessage());
			status = false;
		}
		return status;
	}

	public boolean isControlUndisplayed(WebDriver driver, String locator) {
		Date date = new Date();
		System.out.println("Start time" + date.toString());
		overrideGlobalTimeout(driver, shortTimeout);
		List<WebElement> elements = driver.findElements(By.xpath(locator));

		if (elements.size() == 0) {
			System.out.println("Element not in DOM");
			System.out.println("End time" + new Date().toString());
			overrideGlobalTimeout(driver, longTimeout);
			return true;
		} else if (elements.size() > 0 && !elements.get(0).isDisplayed()) {
			System.out.println("Element in DOM but not visible/displayed");
			System.out.println("End time" + new Date().toString());
			overrideGlobalTimeout(driver, longTimeout);
			return true;
		} else {
			System.out.println("Element in DOM but visible/displayed");
			overrideGlobalTimeout(driver, longTimeout);
			return false;
		}
	}

	public boolean isControlUndisplayed(WebDriver driver, String locator, String... values) {
		Date date = new Date();
		System.out.println("Start time" + date.toString());
		overrideGlobalTimeout(driver, shortTimeout);
		locator = String.format(locator, (Object[]) values);
		List<WebElement> elements = driver.findElements(By.xpath(locator));

		if (elements.size() == 0) {
			System.out.println("Element not in DOM");
			System.out.println("End time" + new Date().toString());
			overrideGlobalTimeout(driver, longTimeout);
			return true;
		} else if (elements.size() > 0 && !elements.get(0).isDisplayed()) {
			System.out.println("Element in DOM but not visible/displayed");
			System.out.println("End time" + new Date().toString());
			overrideGlobalTimeout(driver, longTimeout);
			return true;
		} else {
			System.out.println("Element in DOM but visible/displayed");
			overrideGlobalTimeout(driver, longTimeout);
			return false;
		}
	}

	public boolean isSelected(WebDriver driver, String locator) {
		element = driver.findElement(By.xpath(locator));
		return element.isSelected();
	}

	public boolean isEnabled(WebDriver driver, String locator) {
		element = driver.findElement(By.xpath(locator));
		return element.isEnabled();
	}

	public void switchtoChildWindowById(WebDriver driver, String parent) {
		Set<String> allWindow = driver.getWindowHandles();
		for (String string : allWindow) {
			if (!string.equals(parent)) {
				driver.switchTo().window(string);
				break;
			}
		}

	}

	public void switchtoChildWindowByTitle(WebDriver driver, String title) {
		Set<String> allWindow = driver.getWindowHandles();
		for (String string : allWindow) {
			driver.switchTo().window(string);
			String titleWin = driver.getTitle();
			if (titleWin.equals(title)) {
				break;
			}
		}

	}

	public boolean closeAllWindowsWithoutParent(WebDriver driver, String parent) {
		Set<String> allWindow = driver.getWindowHandles();
		for (String string : allWindow) {
			if (!string.equals(parent)) {
				driver.switchTo().window(string);
				driver.close();
			}
		}
		driver.switchTo().window(parent);
		if (driver.getWindowHandles().size() == 1)
			return true;

		else
			return false;
	}

	public void swtichToIframe(WebDriver driver, String locator) {
		element = driver.findElement(By.xpath(locator));
		driver.switchTo().frame(element);
	}

	public void backToTopWindow(WebDriver driver) {
		driver.switchTo().defaultContent();
	}

	public void hoverMouseToElement(WebDriver driver, String locator) {
		element = driver.findElement(By.xpath(locator));
		action = new Actions(driver);
		action.moveToElement(element).perform();
	}

	public void hoverMouseToElement(WebDriver driver, String locator, String... values) {
		locator = String.format(locator, (Object[]) values);
		element = driver.findElement(By.xpath(locator));
		action = new Actions(driver);
		action.moveToElement(element).perform();
	}

	public void doubleClickToElement(WebDriver driver, String locator) {
		element = driver.findElement(By.xpath(locator));
		action = new Actions(driver);
		action.doubleClick(element).perform();
	}

	public void rightClickToElement(WebDriver driver, String locator) {
		element = driver.findElement(By.xpath(locator));
		action = new Actions(driver);
		action.contextClick(element).perform();
	}

	public void dragAndDrop(WebDriver driver, String locatorDrag, String locatorDrop) {
		elementDrag = driver.findElement(By.xpath(locatorDrag));
		elementDrop = driver.findElement(By.xpath(locatorDrop));
		action = new Actions(driver);
		action.dragAndDrop(elementDrag, elementDrop);
	}

	public void sendkeyBoardToElement(WebDriver driver, String locator, Keys key, String... values) {
		locator = String.format(locator, (Object[]) values);
		element = driver.findElement(By.xpath(locator));
		action = new Actions(driver);
		action.sendKeys(element, key).perform();
	}

	public void sendkeyBoardToElement(WebDriver driver, String locator, Keys key) {
		element = driver.findElement(By.xpath(locator));
		action = new Actions(driver);
		action.sendKeys(element, key).perform();
	}

	public void sendkeyBoard(WebDriver driver, Keys key) {
		action = new Actions(driver);
		action.sendKeys(key);
	}

	public void highlightElement(WebDriver driver, String locator) {
		element = driver.findElement(By.xpath(locator));
		javascriptExecutor = (JavascriptExecutor) driver;
		String originalStyle = element.getAttribute("style");
		javascriptExecutor.executeScript("arguments[0].setAttribute(arguments[1], arguments[2])", element, "style",
				"border: 5px solid red; border-style: dashed;");
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		javascriptExecutor.executeScript("arguments[0].setAttribute(arguments[1], arguments[2])", element, "style",
				originalStyle);

	}

	public Object executeForBrowser(WebDriver driver, String javaSript) {
		javascriptExecutor = (JavascriptExecutor) driver;
		return javascriptExecutor.executeScript(javaSript);
	}

	public boolean verifyTextInInnerText(WebDriver driver, String textExpected) {
		javascriptExecutor = (JavascriptExecutor) driver;
		String textActual = (String) javascriptExecutor
				.executeScript("return document.documentElement.innerText.match('" + textExpected + "')[0]");
		System.out.println("Text actual = " + textActual);
		return textActual.equals(textExpected);
	}

	public Object clickToElementByJS(WebDriver driver, String locator) {
		element = driver.findElement(By.xpath(locator));
		javascriptExecutor = (JavascriptExecutor) driver;
		return javascriptExecutor.executeScript("arguments[0].click();", element);
	}

	public Object scrollToElement(WebDriver driver, String locator) {
		element = driver.findElement(By.xpath(locator));
		javascriptExecutor = (JavascriptExecutor) driver;
		return javascriptExecutor.executeScript("arguments[0].scrollIntoView(true);", element);
	}

	public Object sendkeyToElementByJS(WebDriver driver, String locator, String value) {
		element = driver.findElement(By.xpath(locator));
		javascriptExecutor = (JavascriptExecutor) driver;
		return javascriptExecutor.executeScript("arguments[0].setAttribute('value', '" + value + "')", element);
	}

	public Object scrollToBottomPage(WebDriver driver) {
		javascriptExecutor = (JavascriptExecutor) driver;
		return javascriptExecutor.executeScript("window.scrollBy(0,document.body.scrollHeight)");
	}

	public Object removeAttributeInDOM(WebDriver driver, String locator, String attributeRemove) {
		element = driver.findElement(By.xpath(locator));
		javascriptExecutor = (JavascriptExecutor) driver;
		return javascriptExecutor.executeScript("arguments[0].removeAttribute('" + attributeRemove + "');", element);
	}

	public Object navigateToUrlByJS(WebDriver driver, String url) {
		javascriptExecutor = (JavascriptExecutor) driver;
		return javascriptExecutor.executeScript("window.location = '" + url + "'");
	}

	public void waitForElementPresence(WebDriver driver, String locator) {
		waitExplicit = new WebDriverWait(driver, longTimeout);
		byLocator = By.xpath(locator);
		waitExplicit.until(ExpectedConditions.presenceOfElementLocated(byLocator));
	}
	
	public void waitForElementPresence(WebDriver driver, String locator, String... values) {
		waitExplicit = new WebDriverWait(driver, longTimeout);
		locator = String.format(locator, (Object[]) values);
		byLocator = By.xpath(locator);
		waitExplicit.until(ExpectedConditions.presenceOfElementLocated(byLocator));
	}

	public void waitForElementVisible(WebDriver driver, String locator) {
//			highlightElement(driver, locator);
		waitExplicit = new WebDriverWait(driver, longTimeout);
		byLocator = By.xpath(locator);
		try {
			waitExplicit.until(ExpectedConditions.visibilityOfElementLocated(byLocator));
		} catch (Exception ex) {
			Reporter.log("============================================================Wait for element not visible");
			Reporter.log(ex.getMessage());
			System.err.println(ex.getMessage() + "\n");
		}

	}

	public void waitForElementVisible(WebDriver driver, String locator, String... values) {
		waitExplicit = new WebDriverWait(driver, longTimeout);
		locator = String.format(locator, (Object[]) values);
//			highlightElement(driver, locator);
		byLocator = By.xpath(locator);
		waitExplicit.until(ExpectedConditions.visibilityOfElementLocated(byLocator));
	}

	public void waitForElementClickAble(WebDriver driver, String locator) {
		waitExplicit = new WebDriverWait(driver, longTimeout);
		byLocator = By.xpath(locator);
		waitExplicit.until(ExpectedConditions.elementToBeClickable(byLocator));
	}

	public void waitForElementClickAble(WebDriver driver, String locator, String... values) {
		waitExplicit = new WebDriverWait(driver, longTimeout);
		locator = String.format(locator, (Object[]) values);
		byLocator = By.xpath(locator);
		waitExplicit.until(ExpectedConditions.elementToBeClickable(byLocator));
	}

	public void waitForElementInVisible(WebDriver driver, String locator) {
		Date date = new Date();
		byLocator = By.xpath(locator);
		waitExplicit = new WebDriverWait(driver, shortTimeout);
		overrideGlobalTimeout(driver, shortTimeout);
		System.out.println("Start time for wait invisible =" + date.toString());
		waitExplicit.until(ExpectedConditions.invisibilityOfElementLocated(byLocator));
		System.out.println("End time for wait invisible =" + new Date().toString());
		overrideGlobalTimeout(driver, longTimeout);
	}

	public void waitForElementAlertPresence(WebDriver driver) {
		waitExplicit = new WebDriverWait(driver, longTimeout);
		waitExplicit.until(ExpectedConditions.alertIsPresent());
	}

	public void overrideGlobalTimeout(WebDriver driver, long timeOut) {
		driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
	}

	public void sleepInSecond(long timeInSecond) {
		try {
			Thread.sleep(timeInSecond * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
}
