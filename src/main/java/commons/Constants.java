package commons;

public class Constants {
	
	public static final String DYNAMIC_REEBONZ_ENVIRONMENT_URL = "https://%s-www.reebonz-dev.com/sg";
	public static final String TITAN_URL = "http://credit.uat.titan.reebonz-dev.com/credit_admin";
	public static final String DYNAMIC_MERCHANT_ADMIN_URL = "https://%s-merchants.reebonz-dev.com";
	public static final int SHORT_TIMEOUT = 5;
	public static final int LONG_TIMEOUT = 120;

}
