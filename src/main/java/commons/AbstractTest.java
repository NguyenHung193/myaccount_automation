package commons;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.URL;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Store;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeSuite;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;

public class AbstractTest {

	protected static AndroidDriver<AndroidElement> driver;
	public static AppiumDriverLocalService service;
	protected final Log log;
	private final static String workingDir = System.getProperty("user.dir");

	protected AbstractTest() {
		log = LogFactory.getLog(getClass());
	}

	public AndroidDriver<AndroidElement> getDriver() {
		return driver;
	}

	@BeforeSuite
	public void deleteAllFilesInReportNGScreenShot() {
		System.out.println("--------------------------- Start Delete All Files ---------------------------");
		deleteAllFileInFolder();
	}

	public void deleteAllFileInFolder() {
		try {
			String pathFolderDownload = workingDir + "\\ReportNGScreenShots";
			File file = new File(pathFolderDownload);
			File[] listOfFiles = file.listFiles();
			for (int i = 0; i < listOfFiles.length; i++) {
				if (listOfFiles[i].isFile()) {
					new File(listOfFiles[i].toString()).delete();
				}
			}
		} catch (Exception e) {
			System.out.print(e.getMessage());
		}
	}

	public AppiumDriverLocalService startServer() {

		//
		boolean flag = checkIfServerIsRunnning(4723);
		if (!flag) {

			service = AppiumDriverLocalService.buildDefaultService();
			service.start();
		}
		return service;

	}

	public static boolean checkIfServerIsRunnning(int port) {

		boolean isServerRunning = false;
		ServerSocket serverSocket;
		try {
			serverSocket = new ServerSocket(port);

			serverSocket.close();
		} catch (IOException e) {
			// If control comes here, then it means that the port is in use
			isServerRunning = true;
		} finally {
			serverSocket = null;
		}
		return isServerRunning;
	}

	public static void startEmulator() {

		try {
			Runtime.getRuntime().exec(workingDir + "\\src\\main\\java\\startEmulator.bat");
			Thread.sleep(6000);
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void killAllNodes() {
		try {
			Runtime.getRuntime().exec("taskkill /F /IM node.exe");
			Thread.sleep(3000);
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected AndroidDriver<AndroidElement> openAndroidDevice(String appName) {

		try {
			FileInputStream fis = new FileInputStream(workingDir + "\\src\\main\\java\\global.properties");
			Properties prop = new Properties();
			prop.load(fis);

			File f = new File(workingDir + "\\src\\main\\resources\\appInstall");
			File fs = new File(f, (String) prop.get(appName));
			DesiredCapabilities cap = new DesiredCapabilities();
			String device = (String) prop.get("GooglePixel3");

			if (device.contains("Bi")) {
				startEmulator();
				System.out.println("+++++++++++++++++++++++++++++++++++++++++++++");
			}

			cap.setCapability(MobileCapabilityType.DEVICE_NAME, device);
			cap.setCapability(MobileCapabilityType.AUTOMATION_NAME, "uiautomator2");
			cap.setCapability("noResetValue","false");
			cap.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 10000);

			cap.setCapability(MobileCapabilityType.APP, fs.getAbsolutePath());
			driver = new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"), cap);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			return driver;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return driver;
		}
	}

//	public static void getScreenshot(String s) throws IOException {
//		File scrfile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
//		FileUtils.copyFile(scrfile, new File(System.getProperty("user.dir") + "\\" + s + ".png"));
//
//	}

	private boolean checkPassed(boolean condition) {
		boolean pass = true;
		try {
			if (condition == true)
				log.info("===PASSED==");
			else
				log.info("===FAILED==");
			Assert.assertTrue(condition);
		} catch (Throwable e) {
			pass = false;

			// Add lỗi vào ReportNG
			VerificationFailures.getFailures().addFailureForTest(Reporter.getCurrentTestResult(), e);
			Reporter.getCurrentTestResult().setThrowable(e);
		}
		return pass;
	}

	protected boolean verifyTrue(boolean condition) {
		return checkPassed(condition);
	}

	private boolean checkFailed(boolean condition) {
		boolean pass = true;
		try {
			if (condition == false)
				log.info("===PASSED===");
			else
				log.info("===FAILED===");
			Assert.assertFalse(condition);
		} catch (Throwable e) {
			pass = false;
			VerificationFailures.getFailures().addFailureForTest(Reporter.getCurrentTestResult(), e);
			Reporter.getCurrentTestResult().setThrowable(e);
		}
		return pass;
	}

	protected boolean verifyFalse(boolean condition) {
		return checkFailed(condition);
	}

	private boolean checkEquals(Object actual, Object expected) {
		boolean pass = true;
		boolean status;
		try {
			if (actual instanceof String && expected instanceof String) {
				actual = actual.toString().trim();
				log.info("Actual = " + actual);
				expected = expected.toString().trim();
				log.info("Expected = " + expected);
				status = (actual.equals(expected));
			} else {
				status = (actual == expected);
			}

			log.info("Compare value = " + status);
			if (status) {
				log.info("===PASSED===");
			} else {
				log.info("===FAILED===");
			}
			Assert.assertEquals(actual, expected, "Value is not matching!");
		} catch (Throwable e) {
			pass = false;
			VerificationFailures.getFailures().addFailureForTest(Reporter.getCurrentTestResult(), e);
			Reporter.getCurrentTestResult().setThrowable(e);
		}
		return pass;
	}

	protected boolean verifyEquals(Object actual, Object expected) {
		return checkEquals(actual, expected);
	}

//	------------------------------------------

	public String verifyMail(String userName, String password, String message) {
		Folder folder = null;
		Store store = null;
		System.out.println("***READING MAILBOX...");
		try {
			Properties props = new Properties();
			props.put("mail.store.protocol", "imaps");
			Session session = Session.getInstance(props);
			store = session.getStore("imaps");
			store.connect("imap.gmail.com", userName, password);
			folder = store.getFolder("INBOX");
			folder.open(Folder.READ_ONLY);
			Message[] messages = folder.getMessages();
			System.out.println("No of Messages : " + folder.getMessageCount());
			System.out.println("No of Unread Messages : " + folder.getUnreadMessageCount());
			Folder[] f = store.getDefaultFolder().list();
			for(Folder fd:f)
			    System.out.println(">> "+fd.getName());
			for (int i = messages.length - 1; messages.length - i < 10; i--) {
				System.out.println("Reading MESSAGE # " + (i + 1) + "...");
				Message msg = messages[i];
				String strMailSubject = "", strMailBody = "";
				
				
				Object content = msg.getContent();  
				if (content instanceof String)  
				{  
				    String mp = (String) msg.getContent();  
					String bp = mp.toString();
					// Getting mail subject
					Object subject = msg.getSubject();
					// Getting mail body
//					Object content = msg.getContent();
					// Casting objects of mail subject and body into String
					strMailSubject = (String) subject;
					System.out.println(strMailSubject);
					// ---- This is what you want to do------
					if (strMailSubject.contains(message)) {
						System.out.println(strMailSubject);
//						System.out.println(bp);
						return bp;
//						break;
					}
				}  
				else if (content instanceof Multipart)  
				{  
					Multipart mp = (Multipart) msg.getContent();
					BodyPart bp = mp.getBodyPart(0);
					// Getting mail subject
					Object subject = msg.getSubject();
					// Getting mail body
//					Object content = msg.getContent();
					// Casting objects of mail subject and body into String
					strMailSubject = (String) subject;
					System.out.println(strMailSubject);
					// ---- This is what you want to do------
					if (strMailSubject.contains(message)) {
						System.out.println(strMailSubject);
//						System.out.println(bp.getContent().toString());
						return bp.getContent().toString();
//						break;
					}
				} 
				
				
//				Multipart mp = (Multipart) msg.getContent();
//				BodyPart bp = mp.getBodyPart(0);
//				// Getting mail subject
//				Object subject = msg.getSubject();
//				// Getting mail body
////				Object content = msg.getContent();
//				// Casting objects of mail subject and body into String
//				strMailSubject = (String) subject;
//				System.out.println(strMailSubject);
//				// ---- This is what you want to do------
//				if (strMailSubject.contains(message)) {
//					System.out.println(strMailSubject);
//					System.out.println(bp.getContent());
//					break;
//				}
			}
//			return true;
		} catch (MessagingException messagingException) {
			messagingException.printStackTrace();
		} catch (IOException ioException) {
			ioException.printStackTrace();
		} finally {
			if (folder != null) {
				try {
					folder.close(true);
				} catch (MessagingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (store != null) {
				try {
					store.close();
				} catch (MessagingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
//		return false;
		return "No mail";
	}
	
	public int randomNumber() {
		Random random = new Random();
		return random.nextInt(999999);
	}
}
