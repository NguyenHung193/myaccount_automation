package myaccountant.pageObjects;

import commons.AbstractPage;
import commons.PageFactoryManage;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import myaccountantUI.AuthenticationPageUI;
import myaccountantUI.LoginPageUI;
import myaccountantUI.ReportPageUI;
import myaccountantUI.SignUpPageUI;
import testData.ReportPageData;

public class ReportPageObject extends AbstractPage {

	private AndroidDriver<AndroidElement> driver;

	public ReportPageObject(AndroidDriver<AndroidElement> driver) {
		this.driver = driver;
	}
	
	public double getDynamicValueNet_Profit_2_ReportValue(String value) {
		waitForElementVisible(driver, ReportPageUI.DYNAMIC_VALUE_NET_PROFIT_2,value);
		return Double.valueOf(getAttributeValue(driver, ReportPageUI.DYNAMIC_VALUE_NET_PROFIT_2, "text", value).replaceAll("[^0-9]", ""));
	}
	
	public double getDynamicValueNet_Profit_1_ReportValue(String value) {
		waitForElementVisible(driver, ReportPageUI.DYNAMIC_VALUE_NET_PROFIT_1,value);
		return Double.parseDouble(getAttributeValue(driver, ReportPageUI.DYNAMIC_VALUE_NET_PROFIT_1, "text", value).replaceAll("[^0-9]", ""));
	}
	
	public void moveToDynamicElement(String local) {
		moveToElement(driver, local);
	}
	
	public void isTotalIncomeDisplayed () {
		waitForElementVisible(driver, ReportPageUI.Income);
	}
}
