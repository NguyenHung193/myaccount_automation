package myaccountant.pageObjects;

import commons.AbstractPage;
import commons.PageFactoryManage;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import myaccountantUI.DashboardPageUI;
import myaccountantUI.SettingsPageUI;

public class InvoicePageObject extends AbstractPage {

	private AndroidDriver<AndroidElement> driver;

	public InvoicePageObject(AndroidDriver<AndroidElement> driver) {
		this.driver = driver;
	}

	public void clickToFabButton() {
		waitForElementVisible(driver, DashboardPageUI.FAB_BUTTON);
		tapToElement(driver, DashboardPageUI.FAB_BUTTON);
//		clickToElement(driver, DashboardPageUI.FAB_BUTTON);
	}
	
	public AddItemPageObject clickToAddItemButton() {
		waitForElementVisible(driver, SettingsPageUI.ADD_ITEM_BUTTON);
		tapToElement(driver, SettingsPageUI.ADD_ITEM_BUTTON);
		return PageFactoryManage.getItemPage(driver);
	}
	
	public AddCustomerPageObject clickToAddCustomerButton() {
		waitForElementVisible(driver, SettingsPageUI.ADD_CUSTOMER_BUTTON);
		tapToElement(driver, SettingsPageUI.ADD_CUSTOMER_BUTTON);
		return PageFactoryManage.getCustomerPage(driver);
	}
	
	public AddInvoicePageObject clickToAddInvoiceButton() {
		waitForElementVisible(driver, SettingsPageUI.ADD_INVOICE_BUTTON);
		tapToElement(driver, SettingsPageUI.ADD_INVOICE_BUTTON);
		return PageFactoryManage.getInvoicePage(driver);
	}

}
