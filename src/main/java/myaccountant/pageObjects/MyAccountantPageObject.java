package myaccountant.pageObjects;

import commons.AbstractPage;
import commons.PageFactoryManage;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import myaccountantUI.AuthenticationPageUI;
import myaccountantUI.LoginPageUI;
import myaccountantUI.MyAccountantPageUI;

public class MyAccountantPageObject extends AbstractPage {

	private AndroidDriver<AndroidElement> driver;

	public MyAccountantPageObject(AndroidDriver<AndroidElement> driver) {
		this.driver = driver;
	}
	
	public void clickToOkButton () {
		waitForElementVisible(driver, MyAccountantPageUI.OK_BUTTON);
		clickToElement(driver, MyAccountantPageUI.OK_BUTTON);
	}
	
	public void clickAddMyAccountantButton () {
		waitForElementVisible(driver, MyAccountantPageUI.ADD_ACCOUNTANT_BUTTON);
		clickToElement(driver, MyAccountantPageUI.ADD_ACCOUNTANT_BUTTON);
	}
	
	public void clickFirstAccountantButton () {
		waitForElementVisible(driver, MyAccountantPageUI.FIRST_ACCOUNTANT);
		clickToElement(driver, MyAccountantPageUI.FIRST_ACCOUNTANT);
	}
	
	public void clickInviteButton () {
		waitForElementVisible(driver, MyAccountantPageUI.INVITE_BUTTON);
		clickToElement(driver, MyAccountantPageUI.INVITE_BUTTON);
	}
	
	public void clickConfirmButton () {
		waitForElementVisible(driver, MyAccountantPageUI.CONFIRM_BUTTON);
		clickToElement(driver, MyAccountantPageUI.CONFIRM_BUTTON);
	}
	
	public void clickBackButton () {
		waitForElementVisible(driver, MyAccountantPageUI.BACK_BUTTON);
		clickToElement(driver, MyAccountantPageUI.BACK_BUTTON);
	}
	
	public void clickToCloseListAccountantButton () {
		waitForElementVisible(driver, MyAccountantPageUI.CLOSE_LIST_ACCOUNT_BUTTON);
		clickToElement(driver, MyAccountantPageUI.CLOSE_LIST_ACCOUNT_BUTTON);
	}
	
	public AuthenticationPageObject clickToCloseIcon () {
		waitForElementVisible(driver, LoginPageUI.CLOSE_BUTTON);
		clickToElement(driver, LoginPageUI.CLOSE_BUTTON);
		return PageFactoryManage.getAuthenticationPage(driver);
	}
}
