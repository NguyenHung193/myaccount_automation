package myaccountant.pageObjects;

import commons.AbstractPage;
import commons.PageFactoryManage;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import myaccountantUI.DashboardPageUI;

public class DashboardPageObject extends AbstractPage {

	private AndroidDriver<AndroidElement> driver;

	public DashboardPageObject(AndroidDriver<AndroidElement> driver) {
		this.driver = driver;
	}

	public void sendkeyToDynamicInput(String locator, String valueToSendkey) {
		waitForElementVisible(driver, locator);
		sendkeyToElement(driver, locator, valueToSendkey);
	}

	public void clickToNoThanksButton() {
		waitForElementVisible(driver, DashboardPageUI.NOTHANKS_POPUP);
		clickToElement(driver, DashboardPageUI.NOTHANKS_POPUP);
	}

	public void clickToFabButton() {
		waitForElementVisible(driver, DashboardPageUI.FAB_BUTTON);
		tapToElement(driver, DashboardPageUI.FAB_BUTTON);
//		clickToElement(driver, DashboardPageUI.FAB_BUTTON);
	}
	
	public AddIncomeExpensePaymentPageObject clickToAddIncomeButton() {
		waitForElementVisible(driver, DashboardPageUI.ADD_INCOME_BUTTON);
		tapToElement(driver, DashboardPageUI.ADD_INCOME_BUTTON);
		return PageFactoryManage.getIncomeExpensePaymentPage(driver);
	}
	
	public AddIncomeExpensePaymentPageObject clickToAddExpenseButton() {
		waitForElementVisible(driver, DashboardPageUI.ADD_EXPENSE_BUTTON);
		tapToElement(driver, DashboardPageUI.ADD_EXPENSE_BUTTON);
		return PageFactoryManage.getIncomeExpensePaymentPage(driver);
	}
	
	public AddIncomeExpensePaymentPageObject clickToAddPaymentButton() {
		waitForElementVisible(driver, DashboardPageUI.ADD_PAYMENT_BUTTON);
		tapToElement(driver, DashboardPageUI.ADD_PAYMENT_BUTTON);
		return PageFactoryManage.getIncomeExpensePaymentPage(driver);
	}

}
