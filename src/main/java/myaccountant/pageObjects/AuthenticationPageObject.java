package myaccountant.pageObjects;

import commons.AbstractPage;
import commons.PageFactoryManage;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import myaccountantUI.AuthenticationPageUI;

public class AuthenticationPageObject extends AbstractPage {

	private AndroidDriver<AndroidElement> driver;

	public AuthenticationPageObject(AndroidDriver<AndroidElement> driver) {
		this.driver = driver;
	}
	
	public LoginPageObject clickToLoginButton () {
		waitForElementVisible(driver, AuthenticationPageUI.LOGIN_BUTTON);
		tapToElement(driver, AuthenticationPageUI.LOGIN_BUTTON);
		return PageFactoryManage.getLoginPage(driver);
	}
	
	public SignUpPageObject clickToSignUpButton () {
		waitForElementVisible(driver, AuthenticationPageUI.SIGNUP_BUTTON);
		tapToElement(driver, AuthenticationPageUI.SIGNUP_BUTTON);
		return PageFactoryManage.getSignUpPage(driver);
	}
	
	public SignUpAsAccountPageObject clickToSignUpAsAccountButton () {
		waitForElementVisible(driver, AuthenticationPageUI.SIGNUP_AS_ACCOUNTANT_BUTTON);
		tapToElement(driver, AuthenticationPageUI.SIGNUP_AS_ACCOUNTANT_BUTTON);
		return PageFactoryManage.getSignUpAsAccountantPage(driver);
	}
}
