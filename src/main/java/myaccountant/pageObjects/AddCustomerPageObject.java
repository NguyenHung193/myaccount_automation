package myaccountant.pageObjects;

import commons.AbstractPage;
import commons.PageFactoryManage;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import myaccountantUI.AddCustomerPageUI;
import myaccountantUI.AddIncomeExpensePaymentPageUI;
import myaccountantUI.AuthenticationPageUI;

public class AddCustomerPageObject extends AbstractPage {

	private AndroidDriver<AndroidElement> driver;

	public AddCustomerPageObject(AndroidDriver<AndroidElement> driver) {
		this.driver = driver;
	}
	
	
	public void sendkeysToFirstName (String value) {
		waitForElementVisible(driver, AddCustomerPageUI.FIRST_NAME);
		clearElement(driver, AddCustomerPageUI.FIRST_NAME, value);
		sendkeyToElement(driver, AddCustomerPageUI.FIRST_NAME, value);
	}
	
	public void sendkeysToLastName (String value) {
		waitForElementVisible(driver, AddCustomerPageUI.LAST_NAME);
		sendkeyToElement(driver, AddCustomerPageUI.LAST_NAME, value);
	}
	
	public void sendkeysToEmail (String value) {
		waitForElementVisible(driver, AddCustomerPageUI.EMAIL);
		sendkeyToElement(driver, AddCustomerPageUI.EMAIL, value);
	}
	
	public void sendkeysToPhone (String value) {
		waitForElementVisible(driver, AddCustomerPageUI.PHONE_NUMBER);
		sendkeyToElement(driver, AddCustomerPageUI.PHONE_NUMBER, value);
	}
	
	public void sendkeysToCustomerBillingName (String value) {
		waitForElementVisible(driver, AddCustomerPageUI.CUSTOMER_BILLING_NAME);
		sendkeyToElement(driver, AddCustomerPageUI.CUSTOMER_BILLING_NAME, value);
	}
	
	public void sendkeysToGSTNumber (String value) {
		waitForElementVisible(driver, AddCustomerPageUI.GST_NUMBER);
		sendkeyToElement(driver, AddCustomerPageUI.GST_NUMBER, value);
	}
	
	public void sendkeysToStreetAddress (String value) {
		waitForElementVisible(driver, AddCustomerPageUI.STREET_ADDRESS);
		sendkeyToElement(driver, AddCustomerPageUI.STREET_ADDRESS, value);
	}
	
	public void sendkeysToCity (String value) {
		waitForElementVisible(driver, AddCustomerPageUI.CITY);
		sendkeyToElement(driver, AddCustomerPageUI.CITY, value);
	}
	
	public void clickToState () {
		waitForElementVisible(driver, AddCustomerPageUI.STATE);
		tapToElement(driver, AddCustomerPageUI.STATE);
	}
	
	public void selectDynamicState (String value) {
		waitForElementVisible(driver, AddCustomerPageUI.SELECT_DYNAMIC_STATE, value);
		tapToElement(driver, AddCustomerPageUI.SELECT_DYNAMIC_STATE, value);
	}
	
	public void sendkeysToPostCode (String value) {
		waitForElementVisible(driver, AddCustomerPageUI.POST_CODE);
		sendkeyToElement(driver, AddCustomerPageUI.POST_CODE, value);
	}
	
	public void sendkeysToCountry (String value) {
		waitForElementVisible(driver, AddCustomerPageUI.COUNTRY);
		sendkeyToElement(driver, AddCustomerPageUI.COUNTRY, value);
	}
	
	public void submitTransaction () {
		waitForElementVisible(driver, AddCustomerPageUI.SUBMIT_BUTTON);
		tapToElement(driver, AddCustomerPageUI.SUBMIT_BUTTON);
	}
	
	public void clickToUseSameAsBilling() {
		waitForElementVisible(driver, AddCustomerPageUI.USE_SAME_AS_BILLING_BUTTON);
		tapToElement(driver, AddCustomerPageUI.USE_SAME_AS_BILLING_BUTTON);
	}
	
	public InvoicePageObject clickToOkButton() {
		waitForElementVisible(driver, AddCustomerPageUI.OK_BUTTON);
		tapToElement(driver, AddCustomerPageUI.OK_BUTTON);
		return PageFactoryManage.getInvoicingPage(driver);
	}
}
