package myaccountant.pageObjects;

import commons.AbstractPage;
import commons.PageFactoryManage;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import myaccountantUI.ActivityPageUI;
import myaccountantUI.DashboardPageUI;

public class ActivityPageObject extends AbstractPage {

	private AndroidDriver<AndroidElement> driver;

	public ActivityPageObject(AndroidDriver<AndroidElement> driver) {
		this.driver = driver;
	}

	public void clickToDynamicTab(String value) {
		waitForElementVisible(driver, ActivityPageUI.DYNAMIC_TAB, value);
		tapToElement(driver, ActivityPageUI.DYNAMIC_TAB, value);
	}
	
	public EditTransactionPageObject clickToDynamicTransaction(String value) {
		waitForElementVisible(driver, ActivityPageUI.DYNAMIC_TRANSACTION, value);
		tapToElement(driver, ActivityPageUI.DYNAMIC_TRANSACTION, value);
		return PageFactoryManage.getEditTransactionPage(driver);
	}
	
	public void clickToFabButton() {
		waitForElementVisible(driver, DashboardPageUI.FAB_BUTTON);
		tapToElement(driver, DashboardPageUI.FAB_BUTTON);
	}
}
