package myaccountant.pageObjects;

import commons.AbstractPage;
import commons.PageFactoryManage;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import myaccountantUI.ActivityPageUI;
import myaccountantUI.AddIncomeExpensePaymentPageUI;
import myaccountantUI.DashboardPageUI;
import myaccountantUI.EditTransactionPageUI;

public class EditTransactionPageObject extends AbstractPage {

	private AndroidDriver<AndroidElement> driver;

	public EditTransactionPageObject(AndroidDriver<AndroidElement> driver) {
		this.driver = driver;
	}

	public void sendkeysToAmount (String value) {
		waitForElementVisible(driver, AddIncomeExpensePaymentPageUI.AMOUNT);
		sendkeyToElement(driver, AddIncomeExpensePaymentPageUI.AMOUNT, value);
	}
	
	public void selectCategoryValue (String value) {
		waitForElementVisible(driver, AddIncomeExpensePaymentPageUI.DYNAMIC_CATEGORY_VALUE,value);
		tapToElement(driver, AddIncomeExpensePaymentPageUI.DYNAMIC_CATEGORY_VALUE,value);
	}
	
	public void selectCategory () {
		waitForElementVisible(driver, AddIncomeExpensePaymentPageUI.CATEGORY_SELECT);
		tapToElement(driver, AddIncomeExpensePaymentPageUI.CATEGORY_SELECT);
	}
	
	public void submitTransaction () {
		waitForElementVisible(driver, EditTransactionPageUI.SUBMIT_BUTTON);
		tapToElement(driver, EditTransactionPageUI.SUBMIT_BUTTON);
	}
	
	public void deleteTransaction () {
		waitForElementVisible(driver, EditTransactionPageUI.DELETE_BUTTON);
		tapToElement(driver, EditTransactionPageUI.DELETE_BUTTON);
	}
	
	public void confirmDeleteTransaction () {
		waitForElementVisible(driver, EditTransactionPageUI.CONFIRM_DELETE_BUTTON);
		tapToElement(driver, EditTransactionPageUI.CONFIRM_DELETE_BUTTON);
	}
	
	public void selectCash () {
		waitForElementVisible(driver, AddIncomeExpensePaymentPageUI.CASH_SELECT);
		tapToElement(driver, AddIncomeExpensePaymentPageUI.CASH_SELECT);
	}
	
	public void selectMoneyIn () {
		waitForElementVisible(driver, AddIncomeExpensePaymentPageUI.MONEY_IN_SELECT);
		tapToElement(driver, AddIncomeExpensePaymentPageUI.MONEY_IN_SELECT);
	}
	
	public ActivityPageObject selectOkButton () {
		waitForElementVisible(driver, AddIncomeExpensePaymentPageUI.OK_BUTTON);
		tapToElement(driver, AddIncomeExpensePaymentPageUI.OK_BUTTON);
		return PageFactoryManage.getActivityPage(driver);
	}
	
	public void selectMoneyOut () {
		waitForElementVisible(driver, AddIncomeExpensePaymentPageUI.MONEY_OUT_SELECT);
		tapToElement(driver, AddIncomeExpensePaymentPageUI.MONEY_OUT_SELECT);
	}
	
	public void selectCredit () {
		waitForElementVisible(driver, AddIncomeExpensePaymentPageUI.CREDIT_SELECT);
		tapToElement(driver, AddIncomeExpensePaymentPageUI.CREDIT_SELECT);
	}
}
