package myaccountant.pageObjects;

import commons.AbstractPage;
import commons.PageFactoryManage;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import myaccountantUI.AuthenticationPageUI;
import myaccountantUI.LoginPageUI;

public class LoginPageObject extends AbstractPage {

	private AndroidDriver<AndroidElement> driver;

	public LoginPageObject(AndroidDriver<AndroidElement> driver) {
		this.driver = driver;
	}
	
	public void sendkeyToDynamicInput (String valueToSendkey , String string) {
		waitForElementVisible(driver, LoginPageUI.EMAIL_PASSWORD_INPUT, string);
		clearElement(driver, LoginPageUI.EMAIL_PASSWORD_INPUT, string);
		sendkeyToElement(driver, LoginPageUI.EMAIL_PASSWORD_INPUT, valueToSendkey , string);
	}
	
	public DashboardPageObject clickToLoginButton () {
		waitForElementVisible(driver, LoginPageUI.LOGIN_BUTTON);
		clickToElement(driver, LoginPageUI.LOGIN_BUTTON);
		return PageFactoryManage.getDashboardPage(driver);
	}
	
	public void clickToLoginButtonNotValid () {
		waitForElementVisible(driver, LoginPageUI.LOGIN_BUTTON);
		clickToElement(driver, LoginPageUI.LOGIN_BUTTON);
	}
	
	public boolean isEmailEmptyMessageDisplayed () {
		return isControlDisplayed(driver, LoginPageUI.EMAIL_EMPTY_MESSAGE);
	}
	
	public boolean isEmailNotCorrectMessageDisplayed () {
		return isControlDisplayed(driver, LoginPageUI.EMAIL_NOT_CORRECT_MESSAGE);
	}
	
	public boolean isEmailNotFoundMessageDisplayed () {
		return isControlDisplayed(driver, LoginPageUI.EMAIL_NOT_FOUND_MESSAGE);
	}
	
	public boolean isPasswordEmptyMessageDisplayed () {
		return isControlDisplayed(driver, LoginPageUI.PASSWORD_EMPTY_MESSAGE);
	}
	
	public boolean isPasswordInCorrectMessageDisplayed () {
		return isControlDisplayed(driver, LoginPageUI.PASSWORD_IN_CORRECT_MESSAGE);
	}
	
	public boolean isPasswordLessThan6DigitMessageDisplayed () {
		return isControlDisplayed(driver, LoginPageUI.PASSWORD_LESS_THAN_6_DIGIT_MESSAGE);
	}
	
	public void clickToOkButton () {
		waitForElementVisible(driver, LoginPageUI.OK_BUTTON);
		clickToElement(driver, LoginPageUI.OK_BUTTON);
	}
	
	public void clickToIcon () {
		waitForElementVisible(driver, LoginPageUI.ICON);
		clickToElement(driver, LoginPageUI.ICON);
	}
	
	public AuthenticationPageObject clickToCloseIcon () {
		waitForElementVisible(driver, LoginPageUI.CLOSE_BUTTON);
		clickToElement(driver, LoginPageUI.CLOSE_BUTTON);
		return PageFactoryManage.getAuthenticationPage(driver);
	}
}
